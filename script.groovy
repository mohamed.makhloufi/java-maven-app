def buildJar() {
    echo "Build Jar File"
    sh "mvn package"
} 

def buildImage() {
    echo "Build Docker Image From The Jar"
    sh "docker build -t mohamedmakhloufi/my-app:1.0 ."
} 

def pushImage(){
    echo "Push Docker Image"
    withCredentials([usernamePassword(credentialsId: 'docker-hub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push mohamedmakhloufi/my-app:1.0"
    }
}

def deployApp() {
    echo 'deploying the application...'
} 

return this